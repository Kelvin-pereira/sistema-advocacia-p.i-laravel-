@extends('layouts.app')

@section('conteudo')
<br>
<div class="main main-raised">
    <h1 class="text-center">Lista De Advogados</h1><hr>

    {{-- link para Adicionar Novo Advogado --}}
    <div class="raw text-right">
        <a href="{{ route('advogado.create')  }}" class="btn btn-info">
            <i class="material-icons">add</i> Add Advogado 
        </a>
    </div>

    {{-- Listagem de Advogados --}}
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
            <th scope="col">Id</th>
            <th scope="col">Nome</th>
            <th scope="col">Registro OAB</th>
            <th scope="col">Ação</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                @foreach ($advogados as $Advogado)
                {{-- {{dd($advogados)}} --}}
                <th>{{ $Advogado->id }}</th>
                <td>{{$Advogado->nome}}</td>
                <td>{{$Advogado->registro_oab}}</td>
                <td> 
                
                    {{-- laravel collective form --}}
                    {{ Form::open([ 'method'  => 'post', 'route' => [ 'advogado.destroy', $Advogado->id ] ]) }}
                    <a href="/edit/{{ $Advogado->id }}" class="btn btn-warning">Editar</a>
                    @csrf   
                    {{ Form::submit('Excluir',['class' => 'btn btn-danger']) }}
                    
                    {{ Form::close() }}
                        

                </td>
                </tr>                
                @endforeach
        </tbody>
    </table>

   

</div>



    
@endsection