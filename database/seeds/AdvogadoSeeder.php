<?php

use Illuminate\Database\Seeder;

class AdvogadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advogados')->insert([
            ['advogados.nome'=>'Kelvin', 'advogados.registro_oab'=>123456
            ,'advogados.email'=>'kelvinteste@gmail.com', 'advogados.telefone'=>'3333-6666'
            , 'advogados.nivel_acesso_id'=>1],
            ['advogados.nome'=>'fernando', 'advogados.registro_oab'=>123456
            ,'advogados.email'=>'fernandoteste@gmail.com', 'advogados.telefone'=>'3333-6666'
            , 'advogados.nivel_acesso_id'=>1],
            ['advogados.nome'=>'Luiz', 'advogados.registro_oab'=>123456
            ,'advogados.email'=>'Luizteste@gmail.com', 'advogados.telefone'=>'3333-6666'
            , 'advogados.nivel_acesso_id'=>1],
            ['advogados.nome'=>'Pedro', 'advogados.registro_oab'=>123456
            ,'advogados.email'=>'Pedroteste@gmail.com', 'advogados.telefone'=>'3333-6666'
            , 'advogados.nivel_acesso_id'=>1],
        ]);
    }
}
