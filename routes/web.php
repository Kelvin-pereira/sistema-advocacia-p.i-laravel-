<?php

//home
Route::get('/','Site\SiteController@index');

//Clientes
Route::get('/CadastroDeCliente','Cliente\ClienteController@index');



//Advogado
// Route::resource('/advogado', 'Advogado\AdvogadoController');  duas contra barra faz para de funcionar o javaScript
Route::get('/index-advo',                      ['uses' => 'Advogado\AdvogadoController@index',   'as' => 'advogado.index']);
Route::get('/form-cadastro-de-advogado',  ['uses' => 'Advogado\AdvogadoController@create',  'as' => 'advogado.create']);
Route::post('/store',                     ['uses' => 'Advogado\AdvogadoController@store',   'as' => 'advogado.store']);
Route::get('/edit/{id}',                  ['uses' => 'Advogado\AdvogadoController@edit',    'as' => 'advogado.edit']);
Route::post('/destroy/{id}',               ['uses' => 'Advogado\AdvogadoController@destroy', 'as' => 'advogado.destroy']);









