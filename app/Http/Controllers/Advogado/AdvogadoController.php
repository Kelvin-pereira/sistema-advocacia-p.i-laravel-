<?php

namespace App\Http\Controllers\Advogado;

use App\Advogado;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdvogadoController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $advogados = Advogado::all();
        return view('Advogado.index', compact('advogados'));
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('advogado.create');
    }
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $advogado = new Advogado();
         $advogado->nome = $request->nome;
         $advogado->registro_oab = $request->registroOAB;
         $advogado->email = $request->email;
         $advogado->telefone = $request->telefone;
         $advogado->nivel_acesso_id = 2;
         $advogado->save();
         //dd($advogado);
         return redirect(route('advogado.index') )->with('success', 'Post Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Advogado  $advogado
     * @return \Illuminate\Http\Response
     */
    public function show(Advogado $advogado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Advogado  $advogado
     * @return \Illuminate\Http\Response
     */
    public function edit(Advogado $advogado)
    {
        return "test";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advogado  $advogado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advogado $advogado)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advogado  $advogado
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       //dd($advogado);                                  verifivar se o id advogado esta vindo.
       $advogado = Advogado::findOrFail($id);            // verifica se id.advogado existe no banco/ caso não exista apresenta erro 404
       $advogado->delete();                              // detela do banco
       return redirect()->route('advogado.index');       // return view('advogado.index'); //isso é errado tem que passar pelo metodo index da controller do advogado para fazer a listagem novamente 
    }
}
